<?php

namespace App\Presenters;

use Nette\Application\BadRequestException;
use Tracy\Debugger;

class ApiStudentPresenter extends ApiPresenter
{
    private $userId;

    public function startup()
    {
        parent::startup();
        $this->checkMandatoryParameters(["userTicket"]);
        $this->stagModel->setTicket($this->getParameter("userTicket"));
        $this->checkUser();
    }

    public function actionHistory() {
        $history = $this->recordModel->getRecordsForUser($this->userId);
        $fullHistory = [];
        foreach ($history as $h) {
            $fullHistory[] = [
                "skill" => [
                    "cs" => $h->ref("specification", "spec_id")->skill,
                    "en" => $h->ref("specification", "spec_id")->skill_en
                ],
                "subject" => [
                    "short" => $h->ref("specification", "spec_id")->subject_stag,
                    "cs" => $h->ref("specification", "spec_id")->subject,
                    "en" => $h->ref("specification", "spec_id")->subject_en
                ],
                "teacher" => [
                    "ucitIdno" => $h->ref("users", "prof_id")->ucitIdno,
                    "fullName" => $h->ref("users", "prof_id")->fullname,
                ],
                "date" => $h->date,
                "deleted" => $h->deleted
            ];
        }
        $this->sendJson([
            "items" => $fullHistory
        ]);
    }

    public function actionLogbook() {
        $user = $this->mainModel->getUserById($this->userId);
        $logbook = [];

        if ($user->specYear) {
            $logbookData = $this->prepareLogbookData($this->userId, $user->specYear);

            foreach ($logbookData[0] as $category) {
                $categoryData = [
                    "number" => $category->position,
                    "name" => $category->name,
                    "name_en" => $category->name_en,
                    "skills" => []
                ];
                foreach ($logbookData[1][$category->id] as $skill) {
                    $categoryData["skills"][] = [
                        "number" => $category->position . "." . $skill->position,
                        "skill" => [
                            "cs" => $skill->skill,
                            "en" => $skill->skill_en
                        ],
                        "subject" => [
                            "short" => $skill->subject_stag,
                            "cs" => $skill->subject,
                            "en" => $skill->subject_en,
                        ],
                        "form" => $skill->form,
                        "done" => isset($logbookData[2][$skill->id]) ? [
                            "date" => $logbookData[2][$skill->id]->date,
                            "teacher" => [
                                "ucitIdno" => $logbookData[2][$skill->id]->ref("users", "prof_id")->ucitIdno,
                                "fullName" => $logbookData[2][$skill->id]->ref("users", "prof_id")->fullname,
                            ]
                        ] : null
                    ];
                }
                $logbook[] = $categoryData;
            }
        }

        $this->sendJson([
            "name" => $user->specYear,
            "items" => $logbook
        ]);
    }


    public function actionInfo() {
        $user = $this->mainModel->getUserById($this->userId);
        $info = [];

        $info["logbook"] = $user->specYear;
        $info["fullName"] = $user->fullname;
        if ($user->specYear) {
            $specification = count($this->specModel->getSpecificationForYear($user->specYear));
            $done = count($this->recordModel->getRecordsForUser($this->getUser()->getId(), null, true));
            $info["skillsCount"] = $specification;
            $info["skillsDone"] = $done;
        }

        $this->sendJson([
            "userInfo" => $info
        ]);
    }

    private function checkUser() {
        try {
            $userInfos = $this->stagModel->getUserInfo();
            $userInfo = $userInfos[0];
            $externalLogin = $userInfos[1];
            $fullName = $userInfos[2];

            $this->userId = $this->mainModel->getUser(
                $externalLogin,
                $userInfo["userName"],
                $userInfo["role"],
                $userInfo["ucitIdno"],
                $fullName);
        } catch (BadRequestException $e) {
            $this->getHttpResponse()->setCode(401);
            $this->sendJson([
                "code" => 401,
                "message" => "Given user ticket not active."
            ]);
        } catch (\Exception $e) {
            $this->getHttpResponse()->setCode($e->getCode());
            $this->sendJson([
                "code" => $e->getCode(),
                "message" => $e->getMessage()
            ]);
        }
    }
}
