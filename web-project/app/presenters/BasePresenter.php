<?php

namespace App\Presenters;

use App\Components\contentComponentFactoryTrait;
use Nette;
use App\Model;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use contentComponentFactoryTrait;

    /** @var  Model\Main @inject */
    public $mainModel;

    /** @var  Model\Stag @inject */
    public $stagModel;

    /** @var  Model\Api @inject */
    public $apiModel;

    /** @var  Model\Spec @inject */
    public $specModel;

    /** @var  Model\Record @inject */
    public $recordModel;

    const ROLES = ["ST", "VY", "AD"];

    public function beforeRender()
    {
        $this->template->host = $this->getSiteURL();
        $this->template->roles = $this::ROLES;
        if ($this->getUser()->isLoggedIn() && isset($this->getUser()->getIdentity()->data["expired"])) {
            $exception = new Nette\Application\BadRequestException("", 401);
            throw $exception;
        }
        $this->template->addFilter("fullname", function ($userInfo) {
            return $this->stagModel->getFullName($userInfo);
        });
        if ($this->isAjax()) {
            $this->redrawControl("menu");
            $this->redrawControl("flashes");
            $this->redrawControl("content");
        }
        parent::beforeRender();
    }

    protected function createComponentDemoLogin() {
        $form = new Nette\Application\UI\Form();

        $form->addText("name", "Jméno")->setRequired(true);
        $form->addSelect("role", "Role", $this::ROLES);

        $form->addSubmit("submit", "Login");

        $form->onSuccess[] = [$this, "onDemoLogin"];

        return $form;
    }

    protected function checkLoggedIn() {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect("Home:default");
        }
    }

    protected function checkRole($role) {
        $this->checkLoggedIn();
        if (!$this->getUser()->isInRole($role)) {
            $exception = new Nette\Application\BadRequestException("", 403);
            throw $exception;
        }
    }

    public function onDemoLogin($form, $values) {
        $this->getUser()->logout();
        $this->getUser()->login($values["name"],$this::ROLES[$values["role"]], true, $values["name"]);
    }

    public function prepareLogbookData($userId, $specYear) {
        $records = $this->recordModel->getRecordsForUser($userId, null, true);
        $categories = $this->specModel->getCategoriesForYear($specYear);

        $recordBySpec = [];
        foreach ($records as $record) {
            $recordBySpec[$record["spec_id"]] = $record;
        }

        $specification = [];
        //todo: optimize db requests
        foreach ($categories as $c) {
            $specification[$c->id] = $this->specModel->getSpecificationForCategory($c->id);
        }

        return [$categories, $specification, $recordBySpec];
    }

    private function getSiteURL() {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol.$domainName;
    }

    public function createComponentContent(){
        return $this->contentComponentFactory;
    }

}
