<?php

namespace App\Presenters;

use Nette\Application\UI;

class TeacherPresenter extends BasePresenter
{
    private $subjects;

    public function beforeRender()
    {
        $this->checkRole($this::ROLES[1]);
        $user = $this->mainModel->getUserById($this->getUser()->getId());
        $this->stagModel->setTicket($user->ticket);
        $this->subjects = $this->stagModel->getSubjectsForTeacher($user->ucitIdno);

        $this->template->specificationCount = $this->specModel->getSpecificationCountBySubjects($this->subjects);
        parent::beforeRender();
    }

    public function renderDefault() {
        $t = $this->template;

        $history = $this->recordModel->getRecordsByUser($this->getUser()->getId(), 5);

        $t->historyCount = count($history);
        $t->history = $history;
    }
    public function renderHistory() {
        $t = $this->template;

        $history = $this->recordModel->getRecordsByUser($this->getUser()->getId());

        $t->historyCount = count($history);
        $t->history = $history;
    }

    public function renderSkillList($filtr){
        $t = $this->template;
        $availableSkills = $this->specModel->getSpecificationBySubjects($this->subjects, $filtr);
        $t->availableSkills = $availableSkills;
    }


    public function renderStudents($filtr){
        $t = $this->template;
        $students = [];
        if ($filtr) {
            $students = $this->mainModel->findUsersByFullname($filtr);
        }
        $t->filtr = $filtr;
        $t->students = $students;
    }

    public function renderSkill($skillId){
        $t = $this->template;

        $selectedSkill = $this->getSkillById($skillId);
        if (!$selectedSkill) {
            $this->redirect(":skillList");
        }
        $t->selectedSkill = $selectedSkill;
        $stagStudents = $this->stagModel->getStudentsForClass($selectedSkill->subject_stag);

        $osCisla = [];
        foreach ($stagStudents['studentPredmetu'] as $student) {
            $osCisla[] = $student['osCislo'];
        }

        $students = $this->specModel->getStudentsByOsCisla($osCisla);
        $studentsByOsCislo = [];
        $studentIds = [];
        foreach ($students as $student) {
            $studentIds[$student["osCislo"]] = $student["id"];
            $studentsByOsCislo[$student["osCislo"]] = $student;
        }

        $records = $this->recordModel->getRecordsBySkill($skillId);
        $recordsByUser = [];
        foreach ($records as $record) {
            $recordsByUser[$record["stud_id"]] = $record;
        }
        $t->students = $stagStudents;
        $t->studentsIds = $studentIds;
        $t->studentsData = $studentsByOsCislo;
        $t->redordsData = $recordsByUser;

        $availableSkills = $this->specModel->getSpecificationBySubjects($this->subjects);
        $t->availableSkills = $availableSkills;
    }

    private function getSkillById($skillId) {
        if (!$skillId) {
            return false;
        }
        $availableSkills = $this->specModel->getSpecificationBySubjects($this->subjects);
        foreach ($availableSkills as $skill) {
            if ($skill->id == $skillId) {
                return $skill;
            }
        }
        return false;
    }

    public function actionRemoveSkill($recordId, $skillId) {
        if (!$recordId) {
            return;
        }
        $this->recordModel->removeRecord($recordId, $this->getUser()->getId());
        if ($skillId) {
            $this->redirect("Teacher:skill", $skillId);
        } else {
            $this->redirect("Teacher:history");
        }
    }
    public function actionAddSkill($skillId, $studentId, $skillPage) {
        $user = $this->mainModel->getUserById($this->getUser()->getId());
        $this->stagModel->setTicket($user->ticket);
        $this->subjects = $this->stagModel->getSubjectsForTeacher($user->ucitIdno);
        $skill = $this->getSkillById($skillId);
        if (!$skill || !$studentId) {
            return;
        }
        $this->recordModel->addRecord($skillId, $this->getUser()->getId(), $studentId);
        if ($skillPage) {
            $this->redirect("Teacher:skill", $skillId);
        } else {
            $this->redirect("Student:logbook", $studentId);
        }
    }

    protected function createComponentSkillSearch() {
        $form = new UI\Form();

        $form->addText("skill", "Dovednost:")
            ->setAttribute("placeholder", "Název dovednosti")
            ->setDefaultValue($this->getParameter("filtr"));

        $form->addSubmit("submit", "Hledat");

        $form->onSuccess[] = [$this, "onSkillSearch"];

        return $form;
    }

    public function onSkillSearch($form, $values) {
        $this->redirect(":skillList", $values["skill"]);
    }

    protected function createComponentStudentSearch() {
        $form = new UI\Form();

        $form->addText("student", "Student:")
            ->setAttribute("placeholder", "Jméno nebo příjmení")
            ->setDefaultValue($this->getParameter("filtr"));

        $form->addSubmit("submit", "Hledat");

        $form->onSuccess[] = [$this, "onStudentSearch"];

        return $form;
    }

    public function onStudentSearch($form, $values) {
        $this->redirect(":students", $values["student"]);
    }
}
