<?php
namespace App\Model;

use Nette;
use Nette\Security as NS;
use Nette\Security\IAuthenticator;
use Nette\Security\IIdentity;

/**
 * Class Base
 * @package App\Model
 */
class Authenticator implements IAuthenticator{
    use Nette\SmartObject;
    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * @var Main
     */
    private $mainModel;

    /**
     * Set database
     * @param Nette\Database\Context $db
     * @param Main $main
     */
    public function __construct(Nette\Database\Context $db, Main $main){
        $this->db = $db;
        $this->mainModel = $main;
    }

    /**
     * Performs an authentication and returns IIdentity
     * @param array $credentials
     * @return IIdentity
     */
    function authenticate(array $credentials)
    {
        list($userName, $role, $demo, $fullName) = $credentials;

        $roles = $this->enhanceAdminRole($role);

        if ($demo) {
            $row = $this->mainModel->getUserByStagName($userName);
            return new NS\Identity($row ? $row->id : 0, $roles, ['stagName' => $userName, "fullName" => $fullName]);
        } else {
            $id = $this->mainModel->getUser(null, $userName, $role, null, null);
            return new NS\Identity($id, $roles, ['stagName' => $userName, "fullName" => $fullName]);
        }
    }

    /**
     * @param string $role
     * @return string[]
     */
    private function enhanceAdminRole($role) {
        $roles[] = $role;
        if ($role === "AD") {
            $roles[] = "VY";
        }
        return $roles;
    }
}
