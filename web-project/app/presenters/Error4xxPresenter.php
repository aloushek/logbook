<?php

namespace App\Presenters;

use Nette;
use Nette\Security as NS;

class Error4xxPresenter extends Nette\Application\UI\Presenter
{

	public function startup()
	{
		parent::startup();
		if (!$this->getRequest()->isMethod(Nette\Application\Request::FORWARD)) {
			$this->error();
		}
	}


	public function renderDefault(Nette\Application\BadRequestException $exception)
	{
	    if ($exception->getCode() === 401) {
	        $this->requestLogin();
            $this->template->host = $_SERVER['HTTP_HOST'];
        }
		// load template 403.latte or 404.latte or ... 4xx.latte
		$file = __DIR__ . "/templates/Error/{$exception->getCode()}.latte";
		$this->template->setFile(is_file($file) ? $file : __DIR__ . '/templates/Error/4xx.latte');
	}

	private function requestLogin() {
	    if (isset($this->getUser()->getIdentity()->data["expired"])) {
	        return;
        }
	    $id = $this->getUser()->getId();
        $roles = $this->getUser()->getRoles();
	    $newIdentity = new NS\Identity($id, $roles, ["expired" => true]);
	    $this->getUser()->getStorage()->setIdentity($newIdentity);
    }

}
