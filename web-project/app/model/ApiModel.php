<?php
namespace App\Model;

use Nette;

/**
 * Class Base
 * @package App\Model
 */
class Api {
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * Set database
     * @param Nette\Database\Context $db
     */
    public function __construct(Nette\Database\Context $db){
        $this->db = $db;
    }

    /**
     * @return Nette\Database\Table\IRow[]
     */
    public function getApiList(){
        return $this->db->table('api')->fetchAll();
    }

    /**
     * @param number $id
     * @return Nette\Database\Table\IRow
     */
    public function getApi($id){
        return $this->db->table('api')->get($id);
    }

    /**
     * @param number $id
     */
    public function editApi($id, $values){
        $this->db->table('api')->where("id", $id)->update($values);
    }

    /**
     * @param $values
     * @return mixed
     */
    public function generateApi($values){
        $values["secret"] = Nette\Utils\Random::generate(36);
        return $this->db->table("api")->insert($values)["id"];
    }

    /**
     * @param string $token
     * @return boolean
     */
    public function checkToken($token){
        $row = $this->db->table('api')->where("secret", $token)->where("active", 1)->fetch();
        return $row;
    }


}
