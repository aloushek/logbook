<?php

namespace App\Model;

use Nette;
use Nette\Utils\DateTime;

/**
 * Record model
 * @package App\Model
 */
class Record {
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * Set database
     * @param Nette\Database\Context $db
     */
    public function __construct(Nette\Database\Context $db)
    {
        $this->db = $db;
    }

    /**
     * @param number $userId
     * @param number $limit
     * @param boolean $activeOnly
     * @return Nette\Database\Table\IRow[]
     */
    public function getRecordsForUser($userId, $limit = null, $activeOnly = false)
    {
        $sql = $this->db->table("record")->where("stud_id", $userId)->order("date DESC");
        if ($activeOnly) {
            $sql->where("deleted", false);
        }
        if ($limit) {
            return $sql->limit($limit)->fetchAll();
        }
        return $sql->fetchAll();
    }
    /**
     * @param number $userId
     * @param number $limit
     * @return Nette\Database\Table\IRow[]
     */
    public function getRecordsByUser($userId, $limit = null)
    {
        $sql = $this->db->table("record")->where("prof_id", $userId)->order("date DESC");
        if ($limit) {
            return $sql->limit($limit)->fetchAll();
        }
        return $sql->fetchAll();
    }

    /**
     * @param number[] $userIds
     * @return Nette\Database\Table\IRow[]
     */
    public function getRecordForUsers($userIds)
    {
        $sql = $this->db->table("record")->where("stud_id IN", $userIds);
        return $sql->fetchAll();
    }
    /**
     * @param number $skillId
     * @return Nette\Database\Table\IRow[]
     */
    public function getRecordsBySkill($skillId)
    {
        $sql = $this->db->table("record")->where("spec_id", $skillId);
        return $sql->fetchAll();
    }

    public function addRecord($skillId, $teacherId, $studentId) {
        $this->db->table("record")->insert([
            "spec_id" => $skillId,
            "prof_id" => $teacherId,
            "stud_id" => $studentId,
            "date" => new DateTime()
        ]);
    }

    public function removeRecord($recordId, $teacherId) {
        $sql = $this->db->table("record")
            ->where("id", $recordId)
            ->where("prof_id", $teacherId);
        $sql->update(["deleted" => true]);
    }
}
