<?php

namespace App\Presenters;

use Tracy\Debugger;

class HomePresenter extends BasePresenter
{
    /**
     * @param $stagUserTickets
     * @throws \Nette\Application\AbortException
     */
    public function renderLogin($stagUserTicket) {
        $this->stagModel->setTicket($stagUserTicket);

        try {
            $userInfos = $this->stagModel->getUserInfo();
            $userInfo = $userInfos[0];
            $externalLogin = $userInfos[1];
            $fullName = $userInfos[2];

            $role = $this->resolveUserRole($userInfo);

            $this->mainModel->getUser(
                $externalLogin,
                $userInfo["userName"],
                $role,
                $userInfo["ucitIdno"],
                $fullName,
                $stagUserTicket);
            $this->getUser()->login($userInfo["userName"], $role, false, $fullName);
        } catch (\Exception $e) {
            Debugger::log($e);
            $this->redirect("Home:loginFailed");
            return;
        }
        $this->flashMessage("Přihlášení proběhlo úspěšně");
        $this->redirectAfterLogin($userInfo["role"]);
    }

    /**
     * @param $role
     * @throws \Nette\Application\AbortException
     */
    private function redirectAfterLogin($role) {
        switch ($role) {
            case $this::ROLES[0]:
                $this->redirect("Student:default");
                break;
            case $this::ROLES[1]:
                $this->redirect("Teacher:default");
                break;
            case $this::ROLES[2]:
                $this->redirect("Admin:default");
                break;
            default:
                $this->redirect("Home:default");
        }
    }

    private function resolveUserRole($userInfo) {
        $user = $this->mainModel->getUserByOsCislo($userInfo["userName"]);

        if ($userInfo["role"] === $this::ROLES[2] || ($user && $user->role === $this::ROLES[2])) {
            return $this::ROLES[2];
        }
        if ($userInfo["role"] === $this::ROLES[1] || $userInfo["ucitIdno"]) {
            return $this::ROLES[1];
        }
        return $this::ROLES[0];
    }

    /**
     * Logout user.
     */
    public function actionLogout() {
        $this->getUser()->logout();
        $this->flashMessage('Odhlášení proběhlo úspěšně.');
        $this->redirect("Home:default");
    }
}
