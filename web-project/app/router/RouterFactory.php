<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		//API
        $router[] = new Route('api/student/<action>', 'ApiStudent:');
		$router[] = new Route('api/<action>', 'Api:');

		$router[] = new Route('<presenter>/<action>[/<id>]', 'Home:default');
		return $router;
	}

}
