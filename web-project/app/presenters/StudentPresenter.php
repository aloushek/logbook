<?php

namespace App\Presenters;

use Nette\Application\BadRequestException;
use App\Model;
use Tracy\Debugger;

class StudentPresenter extends BasePresenter
{
    /**
     * @var string
     */
    private $specYear = null;

    /**
     * @var number
     */
    private $userId;

    /**
     * @var boolean
     */
    private $isTeacher;

    /** @var  Model\Pdf @inject */
    public $pdfModel;

    public function beforeRender()
    {
        $this->checkRoles();
        $user = $this->mainModel->getUserById($this->userId);
        if (!$user) {
            $exception = new BadRequestException("", 404);
            throw $exception;
        }
        $this->specYear = $user->specYear;
        $this->template->hasSpecYear = boolval($this->specYear);
        $this->template->userData = $user;
        parent::beforeRender();
    }

    public function renderDefault($id) {
        $t = $this->template;

        if ($this->specYear) {
            $specification = $this->specModel->getSpecificationForYear($this->specYear);
            $history = $this->recordModel->getRecordsForUser($this->userId, 5);
            $done = count($this->recordModel->getRecordsForUser($this->userId, null, true));

            $t->specCount = count($specification);
            $t->historyCount = count($history);
            $t->done = $done;
            $t->history = $history;
        }

        $t->specYear = $this->specYear;
    }

    public function renderHistory($id) {
        $t = $this->template;

        if ($this->specYear) {
//            $specification = $this->specModel->getSpecificationForYear($this->specYear);
            $history = $this->recordModel->getRecordsForUser($this->userId);

            $t->historyCount = count($history);
            $t->history = $history;
        } else {
            $this->redirect("Student:default");
        }
    }

    public function renderLogbook($id) {
        $t = $this->template;

        if ($this->specYear) {
            $logbookData = $this->prepareLogbookData($this->userId, $this->specYear);
            $t->categories = $logbookData[0];
            $t->specs = $logbookData[1];
            $t->records = $logbookData[2];
            if ($this->isTeacher) {
                $user = $this->mainModel->getUserById($this->getUser()->getId());
                $this->stagModel->setTicket($user->ticket);
                $subjects = $this->stagModel->getSubjectsForTeacher($user->ucitIdno);
                $r = [];
                foreach ($subjects["predmetUcitele"] as $subject) {
                    $r[] = strtoupper($subject["katedra"] . "/" . $subject["zkratka"]);
                }
                $t->subjects = $r;
            }
        } else {
            $this->redirect("Student:default");
        }
        $t->specYear = $this->specYear;
    }

    public function actionPdfLogbook($id) {
         $this->sendResponse($this->pdfModel->createPdf("foobar"));
    }

    private function checkRoles() {
        $parameterId = $this->getParameter("id");
        if ($parameterId) {
            $this->checkRole($this::ROLES[1]);
            $this->userId = $parameterId;
            $this->isTeacher = true;
        } else {
            $this->checkRole($this::ROLES[0]);
            $this->userId = $this->getUser()->getId();
            $this->isTeacher = false;
        }
        $this->template->id = $parameterId;
        $this->template->teacher = $this->isTeacher;
    }
}
