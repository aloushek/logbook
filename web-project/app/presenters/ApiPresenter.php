<?php

namespace App\Presenters;

use Nette;
use App\Model;

class ApiPresenter extends Nette\Application\UI\Presenter
{
    /** @var  Model\Main @inject */
    public $mainModel;

    /** @var  Model\Stag @inject */
    public $stagModel;

    /** @var  Model\Api @inject */
    public $apiModel;

    /** @var  Model\Spec @inject */
    public $specModel;

    /** @var  Model\Record @inject */
    public $recordModel;

    public function startup()
    {
        parent::startup();
        $this->checkMandatoryParameters(["key"]);
        if (!$this->apiModel->checkToken($this->getParameter("key"))) {
            $this->getHttpResponse()->setCode(401);
            $this->sendJson([
                "code" => 401,
                "message" => "Given key not found or is not active."
            ]);
        }
    }

    public function actionTest($key) {
        $this->sendJson([
            "appName" => $this->apiModel->checkToken($key)->app_name
        ]);
    }

    protected function checkMandatoryParameters($parameters) {
        foreach ($parameters as $parameter) {
            if (!$this->getParameter($parameter)) {
                $this->getHttpResponse()->setCode(400);
                $this->sendJson([
                    "code" => 400,
                    "message" => "Mandatory parameter is missing: ". $parameter
                ]);
            }
        }
    }

    public function prepareLogbookData($userId, $specYear) {
        $records = $this->recordModel->getRecordsForUser($userId, null, true);
        $categories = $this->specModel->getCategoriesForYear($specYear);

        $recordBySpec = [];
        foreach ($records as $record) {
            $recordBySpec[$record["spec_id"]] = $record;
        }

        $specification = [];
        //todo: optimize db requests
        foreach ($categories as $c) {
            $specification[$c->id] = $this->specModel->getSpecificationForCategory($c->id);
        }

        return [$categories, $specification, $recordBySpec];
    }
}
