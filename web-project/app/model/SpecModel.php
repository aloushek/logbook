<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

/**
 * Specification model
 * @package App\Model
 */
class Spec
{
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * Set database
     * @param Nette\Database\Context $db
     */
    public function __construct(Nette\Database\Context $db)
    {
        $this->db = $db;
    }

    /**
     * @param string[] $osCisla
     * @return array
     */
    public function getStudentsByOsCisla($osCisla)
    {
        return $this->db->table("users")->where("osCislo IN", $osCisla)->fetchAll();
    }

    // CATEGORIES

    /**
     * @return array
     */
    public function getCategoryYears()
    {
        return $this->db->table('category')->group("year")->fetchPairs("year", "year");
    }

    /**
     * @param $id
     * @return false|Nette\Database\Table\ActiveRow
     */
    public function getCategory($id)
    {
        return $this->db->table('category')->get($id);
    }

    /**
     * @param string $year
     * @return int
     */
    public function getStudentsCountForYear($year)
    {
        return $this->db->table("users")->where("specYear", $year)->count("id");
    }

    /**
     * @param string $year
     * @return Nette\Database\Table\IRow[]
     */
    public function getCategoriesForYear($year)
    {
        return $this->db->table("category")->where("year", $year)->order("position")->fetchAll();
    }

    /**
     * @param number $id
     */
    public function removeCategory($id)
    {
        $this->db->table("specification")->where("category_id", $id)->delete();
        $this->db->table("category")->where("id", $id)->delete();
    }

    public function addCategory($name, $name_en, $year)
    {
        $this->db->table("category")->insert([
            "name" => $name,
            "name_en" => $name_en,
            "year" => $year,
            "position" => count($this->getCategoriesForYear($year)) + 1,
            "active" => 1
        ]);
    }

    public function editCategory($categoryId, $name, $name_en)
    {
        $this->db->table("category")->where("id", $categoryId)->update([
            "name" => $name,
            "name_en" => $name_en
        ]);
    }

    public function makeCategories($values)
    {
        $year = $values["year"];
        $i = 1;
        $ids = [];

        while (isset($values["name_" . $i])) {
            $name = $values["name_" . $i];
            if (!empty($name)) {
                $ids[] = $this->db->table("category")->insert([
                    "name" => $name,
                    "name_en" => isset($values["name_en_" . $i]) ? $values["name_en_" . $i] : "",
                    "year" => $year,
                    "position" => $i,
                    "active" => 1
                ])->id;
            }
            $i++;
        }

        return $ids;
    }

    // SPECIFICATION

    public function getSpecification($id)
    {
        return $this->db->table("specification")->get($id);
    }

    public function getSpecificationForCategory($categoryId)
    {
        return $this->db->table("specification")->where("category_id", intval($categoryId))->order("position")->fetchAll();
    }

    public function getSpecificationForYear($year)
    {
        $categories = $this->getCategoriesForYear($year);
        $specifications = $this->db->table("specification")->where("category_id", $categories)->order("position")->fetchAll();
        $specByCategory = [];
        foreach ($specifications as $specification) {
            if (!isset($specByCategory[$specification->category_id])) {
                $specByCategory[$specification->category_id] = [];
            }
            $specByCategory[$specification->category_id][] = $specification;
        }
        return $specByCategory;
    }

    public function getSpecificationCountBySubjects($subjects)
    {
        $subjectArray = $subjects["predmetUcitele"];
        $sql = $this->db->table("specification");
        $sql->whereOr($this->prepareSubjectsForSQL($subjectArray));

        return $sql->count();
    }

    public function getSpecificationBySubjects($subjects, $filtr = null)
    {
        $subjectArray = $subjects["predmetUcitele"];

        $sql = $this->db->table("specification");
        $sql->whereOr($this->prepareSubjectsForSQL($subjectArray));
        if ($filtr) {
            $sql->where("skill LIKE ?", "%" . $filtr . "%");
        }

        return $sql->order("subject_stag")->fetchAll();
    }

    private function prepareSubjectsForSQL($subjectArray)
    {
        $subjects = [];

        if (!count($subjectArray)) {
            return ["subject_stag IS NULL"];
        }

        foreach ($subjectArray as $subject) {
            $subjects[] = "subject_stag LIKE '%" . strtoupper($subject["katedra"] . "/" . $subject["zkratka"]) . "%'";
        }

        return $subjects;
    }

    public function addSpecification($values, $classDetailCs, $classDetailEn, $position = null)
    {
        $toInsert = [
            "skill" => $values["skill"],
            "skill_en" => $values["skill_en"],
            "form" => $values["form"],
            "subject_stag" => strtoupper($values["subject_stag"]),
            "subject" => $classDetailCs,
            "subject_en" => $classDetailEn,
            "category_id" => $values["category_id"],
            "position" => $position ?
                $position :
                count($this->getSpecificationForCategory($values["category_id"])) + 1,
            "active" => 1
        ];
        $this->db->table("specification")->insert($toInsert);
    }


    public function editSpecification($id, $values, $classDetailCs, $classDetailEn)
    {
        $toUpdate = [
            "skill" => $values["skill"],
            "skill_en" => $values["skill_en"],
            "form" => $values["form"],
            "subject_stag" => strtoupper($values["subject_stag"]),
            "subject" => $classDetailCs,
            "subject_en" => $classDetailEn,
        ];
        $this->db->table("specification")->where("id", $id)->update($toUpdate);
    }

    /**
     * @param number $id
     */
    public function removeSpecification($id)
    {
        $this->db->table("specification")->where("id", $id)->delete();
    }

    // BASE
    public function cloneByYear($oldYear, $values)
    {
        $oldCategories = $this->getCategoriesForYear($oldYear);
        $oldSpecification = $this->getSpecificationForYear($oldYear);
        $i = 1;
        foreach ($oldCategories as $oldCategory) {
            $values["name_" . $i] = $oldCategory->name;
            $values["name_en_" . $i] = $oldCategory->name_en;
            $i++;
        }
        $ids = $this->makeCategories($values);

        $categoryIndex = 0;
        foreach ($oldSpecification as $specByCategory) {
            $specPosition = 1;
            foreach ($specByCategory as $spec) {
                $this->addSpecification(
                    [
                        "skill" => $spec["skill"],
                        "skill_en" => $spec["skill_en"],
                        "form" => $spec["form"],
                        "subject_stag" => $spec["subject_stag"],
                        "category_id" => $ids[$categoryIndex]
                    ],
                    $spec["subject"],
                    $spec["subject_en"],
                    $specPosition);
                $specPosition++;
            }
            $categoryIndex++;
        }
    }

    public function reorderCategories($ids) {
        $i = 1;
        foreach ($ids as $id) {
            $this->db->table("category")->where("id", $id)->update([
                "position" => $i
            ]);
            $i++;
        }
    }

    public function reorderSpecifications($ids) {
        $i = 1;
        foreach ($ids as $id) {
            $this->db->table("specification")->where("id", $id)->update([
                "position" => $i
            ]);
            $i++;
        }
    }
}
