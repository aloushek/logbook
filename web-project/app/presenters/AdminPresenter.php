<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class AdminPresenter extends BasePresenter
{

    public function beforeRender()
    {
        $this->checkRole($this::ROLES[2]);
        parent::beforeRender();
    }

    // RENDERS

    public function renderDefault()
    {
        $t = $this->template;
        $categoryYears = $this->specModel->getCategoryYears();
        $studentCounts = [];
        foreach ($categoryYears as $c) {
            $studentCounts[$c] = $this->specModel->getStudentsCountForYear($c);
        }
        $t->reports = $categoryYears;
        $t->counts = $studentCounts;
    }

    public function renderSpecAssign($prijmeni, $jmeno)
    {
        $t = $this->template;

        $this->stagModel->loadTicket($this->getUser()->getId());
        $students = $this->stagModel->searchStudents($prijmeni, $jmeno);
        $years = [];
        if ($students) {
            foreach ($students["student"] as $s) {
                $user = $this->mainModel->getUserByOsCislo($s["osCislo"]);
                if ($user) {
                    $years[$s["osCislo"]] = $user->specYear;
                }
            }
        }
        $t->students = $students;
        $t->years = $years;
    }

    public function renderSpecDetail($year)
    {
        $t = $this->template;
        $categories = $this->specModel->getCategoriesForYear($year);
        $specification = [];

        foreach ($categories as $c) {
            $specification[$c->id] = $this->specModel->getSpecificationForCategory($c->id);
        }

        $t->categories = $categories;
        $t->specs = $specification;
        $t->year = $year;
        $t->canEdit = $this->specModel->getStudentsCountForYear($year) == 0;
    }

    public function renderSpecCategory($id, $year)
    {
        $t = $this->template;
        $t->year = $year;
        $t->canEdit = $this->specModel->getStudentsCountForYear($year) == 0;
    }

    public function renderskillEdit($id, $year)
    {
        $t = $this->template;
        if (!$id) {
            $this->redirect(":specDetail", $year);
        }
        $t->year = $year;
        $t->canEdit = $this->specModel->getStudentsCountForYear($year) == 0;
    }

    public function renderApi()
    {
        $t = $this->template;

        $t->apiList = $this->apiModel->getApiList();
    }

    public function renderApiDetail($id)
    {
        $t = $this->template;

        $t->api = $this->apiModel->getApi($id);
    }

    public function renderEditContent($id, $withEditor = true)
    {
        $this->template->id = $id;
        $this->template->withEditor = $withEditor;
    }

    public function renderYearClone($year)
    {
        if (!$year) {
            $this->redirect("Admin:default");
        }
        $this->template->year = $year;
    }

    // ACTIONS

    public function actionCategoryRemove($id, $year, $last)
    {
        if ($this->specModel->getStudentsCountForYear($year) == 0) {
            $this->specModel->removeCategory($id);
        }

        if ($last) {
            $this->redirect("Admin:default");
        } else {
            $this->flashMessage("Kategorie odstraněna.");
            $this->redirect("Admin:specDetail", $year);
        }
    }

    /**
     * @param $id
     * @param $year
     * @throws Nette\Application\AbortException
     */
    public function actionSpecificationRemove($id, $year)
    {
        if ($this->specModel->getStudentsCountForYear($year) == 0) {
            $this->specModel->removeSpecification($id);
        }

        $this->redirect("Admin:specDetail", $year);
    }

    /**
     * @param $id
     * @param $year
     * @throws Nette\Application\AbortException
     */
    public function actionStudentYearRemove($osCislo)
    {
        $user = $this->mainModel->getUserByOsCislo($osCislo);

        $this->mainModel->updateUser($user->id, ["specYear" => null]);

        $this->redirect("Admin:specAssign");
    }

    /**
     * @param $year
     * @param $ids
     * @throws Nette\Application\AbortException
     */
    public function actionCategoryOrder($year, $ids)
    {
        $parsedIds = explode(",", $ids);
        $this->specModel->reorderCategories($parsedIds);

        $this->redirect("Admin:specDetail", $year);
    }

    /**
     * @param $year
     * @param $ids
     * @throws Nette\Application\AbortException
     */
    public function actionSpecificationOrder($year, $ids)
    {
        $parsedIds = explode(",", $ids);
        $this->specModel->reorderSpecifications($parsedIds);

        $this->redirect("Admin:specDetail", $year);
    }

    // COMPONENTS

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentAddApiForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addText("app_name", "Název aplikace")
            ->setRequired("Název aplikace je povinný.");
        $form->addCheckbox("active", "Aktivní")
            ->setDefaultValue(1);

        $form->addSubmit("submit", "Vygenerovat API klíč");
        $form->onSuccess[] = [$this, "addApiFormSubmit"];

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function addApiFormSubmit($form)
    {
        $values = $form->getValues();
        $id = $this->apiModel->generateApi($values);
        $this->redirect("Admin:apiDetail", $id);
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentEditApiForm()
    {
        $form = new Nette\Application\UI\Form();
        $id = $this->getParameter("id");

        $form->addText("app_name", "Název aplikace")
            ->setRequired("Název aplikace je povinný.");
        $form->addCheckbox("active", "Aktivní");

        $form->addSubmit("submit", "Upravit");
        $form->onSuccess[] = [$this, "editApiFormSubmit"];

        $form->setDefaults($this->apiModel->getApi($id));

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function editApiFormSubmit($form)
    {
        $values = $form->getValues();
        $id = $this->getParameter("id");
        $this->apiModel->editApi($id, $values);

        $this->redirect("Admin:api");
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentMakeDefinitionForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addText("year", "Školní rok (název)")
            ->setRequired("Školní rok (název) je povinný.");

        $form->addSubmit("submit", "Vytvořit");
        $form->onSuccess[] = [$this, "makeDefinitionFormSubmit"];


        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function makeDefinitionFormSubmit($form)
    {
        $values = $form->getValues();
        if (count($this->specModel->getCategoriesForYear($values["year"]))) {
            $this->flashMessage("Zadaný název sestavy je již použit.");
        } else {
            $values["name_1"] = "První kategorie";
            $values["name_en_1"] = "First category";
            $this->specModel->makeCategories($values);
        }

        $this->redirect("Admin:specDetail", $values["year"]);
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentYearCloneForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addText("year", "Nový název sestavy")
            ->setRequired("Název sestavy je povinný.");

        $form->addSubmit("submit", "Klonovat");
        $form->onSuccess[] = [$this, "yearCloneFormSubmit"];


        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function yearCloneFormSubmit($form)
    {
        $values = $form->getValues();
        $oldYear = $this->getParameter("year");

        if (count($this->specModel->getCategoriesForYear($values["year"]))) {
            $this->flashMessage("Zadaný název sestavy je již použit.");
            $this->redirect("Admin:specDetail", $values["year"]);
            return;
        }

        $this->specModel->cloneByYear($oldYear, $values);

        $this->redirect("Admin:specDetail", $values["year"]);
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentAddCategoriesForm()
    {
        $form = new Nette\Application\UI\Form();
        $categoryId = $this->getParameter("id");
        $category = null;

        $form->addText("name", "Název kategorie")
            ->setRequired("Musíte zadat název kategorie");
        $form->addText("name_en", "Category name - EN")
            ->setRequired("Musíte zadat název kategorie");

        if ($categoryId) {
            $category = $this->specModel->getCategory($categoryId);
            $form->setDefaults([
                "name" => $category->name,
                "name_en" => $category->name_en
            ]);
        }
        $form->addSubmit("submit", $category ? "Upravit kategorii" : "Přidat kategorii");
        $form->onSuccess[] = [$this, "addCategoriesFormSubmit"];

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function addCategoriesFormSubmit($form)
    {
        $categoryId = $this->getParameter("id");
        $values = $form->getValues();
        $year = $this->getParameter("year");

        if ($this->specModel->getStudentsCountForYear($year) == 0) {
            if ($categoryId) {
                $this->specModel->editCategory($categoryId, $values["name"], $values["name_en"]);
                $this->flashMessage("Kategorie upravena.");
            } else {
                $this->specModel->addCategory($values["name"], $values["name_en"], $year);
                $this->flashMessage("Kategorie přidána.");
            }
        }


        $this->redirect("Admin:specDetail", $year);
    }

    /**
     * @return Nette\Application\UI\Form
     */
    public function createComponentAddSpecificationForm()
    {
        $specId = $this->getParameter("id");
        $form = new Nette\Application\UI\Form();
        $form->addText("subject_stag", "Stag předmět")
            ->addRule($form::PATTERN,
                "Musí mít tvar 'KAT/PRE,KAT/PRE2', maximalne 3 predmety",
                "([0-z]{1,5}\/[0-z]{1,10},?)([0-z]{1,5}\/[0-z]{1,10},?)?([0-z]{1,5}\/[0-z]{1,10},?)?")
            ->setRequired("Musíte zadat stag předmět");
        $form->addRadioList("form", "Forma", ["P" => "Provést", "V" => "Vidět"])
            ->setDefaultValue("P");
        $form->addText("skill", "Dovednost")
            ->setRequired("Musíte zadat dovednost");
        $form->addText("skill_en", "Dovednost - anglicky")
            ->setRequired("Musíte zadat dovednost");
        $form->addSubmit("submit", "Přidat úkon");
        $form->addHidden("category_id");

        if ($specId) {
            $specification = $this->specModel->getSpecification($specId);
            $form->setDefaults([
                "subject_stag" => $specification->subject_stag,
                "form" => $specification->form,
                "skill" => $specification->skill,
                "skill_en" => $specification->skill_en
            ]);
        }

        $form->onSuccess[] = [$this, "addSpecificationFormSubmit"];

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function addSpecificationFormSubmit($form)
    {
        $values = $form->getValues();
        $specId = $this->getParameter("id");
        $subjects = $this->getSubjectsDetail($values["subject_stag"]);
        $year = $this->getParameter("year");

        if ($this->specModel->getStudentsCountForYear($year) == 0) {
            if (is_array($subjects)) {
                if ($specId) {
                    $this->specModel->editSpecification(
                        $specId,
                        $values,
                        $subjects[0],
                        $subjects[1]
                    );
                    $this->flashMessage("Dovednost upravena");
                } else {
                    $this->specModel->addSpecification(
                        $values,
                        $subjects[0],
                        $subjects[1]
                    );
                    $this->flashMessage("Dovednost přidána");
                }
            } else {
                $this->flashMessage("Předmět " . $subjects . " nenalezen");
            }
        }
        $this->redirect("Admin:specDetail", $year);
    }

    private function getSubjectsDetail($subjectsSring)
    {
        $stagSubjects = explode(",", $subjectsSring);
        $csSubjects = [];
        $enSubjects = [];

        foreach ($stagSubjects as $subject) {
            $temp = $this->stagModel->getClassDetail($subject);
            $csSubjects[] = $temp["nazev"];
            if (!$temp) {
                return $subject;
            }
            $temp = $this->stagModel->getClassDetail($subject, "en");
            $enSubjects[] = $temp["nazev"];
            if (!$temp) {
                return $subject;
            }
        }

        return [implode(", ", $csSubjects), implode(", ", $enSubjects)];
    }

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentAssignSpecForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addSelect("specYear", "Sestava", $this->specModel->getCategoryYears());
        $form->addHidden("osCislo");
        $form->addHidden("fullname");

        $form->addSubmit("submit", "Přiřadit");
        $form->onSuccess[] = [$this, "assignSpecFormSubmit"];

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function assignSpecFormSubmit($form)
    {
        $values = $form->getValues();

        $user = $this->mainModel->getUser(null, $values["osCislo"], "ST", null, $values["fullname"]);

        $this->mainModel->updateUser($user, ["specYear" => $values["specYear"]]);

        $this->redirect("this");
    }

    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSearchStudentForm()
    {
        $form = new Nette\Application\UI\Form();
        $form->addText("prijmeni", "Příjmnení")
            ->setDefaultValue($this->getParameter("prijmeni"))
            ->setRequired(false);
        $form->addText("jmeno", "Jméno")
            ->setDefaultValue($this->getParameter("jmeno"))
            ->setRequired(false);

        $form->addSubmit("submit", "Hledat");
        $form->onSuccess[] = [$this, "searchStudentFormSubmit"];

        return $form;
    }

    /**
     * @param Nette\Application\UI\Form $form
     * @throws Nette\Application\AbortException
     */
    public function searchStudentFormSubmit($form)
    {
        $values = $form->getValues();

        $this->redirect("Admin:specAssign", $values["prijmeni"], $values["jmeno"]);
    }

    /**
     * Edit texts on website
     * @return Form
     */
    protected function createComponentEditContent()
    {
        $form = new Form();

        $text = $this->mainModel->getContentById($this->getParameter("id"));

        $form->addTextArea('text')->setAttribute('id', 'editor')->setDefaultValue($text ? $text->text : "");
        $form->addSubmit('save', 'Uložit')->setAttribute('class', 'btn btn-visual');

        $form->onSubmit[] = [$this, 'editContentSuccess'];
        return $form;
    }

    /**
     * Save edited text
     * @param $form
     * @throws Nette\Application\AbortException
     */
    public function editContentSuccess($form)
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->mainModel->updateContentById($this->getParameter("id"), $form->getValues()["text"]);
            $this->flashMessage("Text byl úspěšně uložen");
            $this->redirect(":default");
        }
    }
}
