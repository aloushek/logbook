window.footerFix = function () {
    var win = window.innerHeight;
    var con = $('body').height();
    var f = $('footer');
    if (f.css('position') === 'absolute') {
        con += 62;
    }
    if (win > con) {
        f.css({
            position: 'absolute'
        });
    } else {
        f.css({
            position: 'relative'
        });
    }
};
(function ($) {
    $.fn.disableSelection = function () {
        return this
            .attr('unselectable', 'on')
            .css('user-select', 'none')
            .on('selectstart', false);
    };
})(jQuery);
$(function ($) {
    $.nette.init();
    if (typeof $.nette.ext('history') === "object") {
        $.nette.ext('history').cache = false;
    }
    $.nette.ext('snippets').applySnippet = function ($el, html) {
        var attr = $($el).attr('data-ajax-ani');
        if (typeof attr !== typeof undefined && attr !== false) {
            $el.html(html).hide().fadeIn();
        } else {
            $el.html(html).hide().show();
        }
        $.nette.load();
    };
    $.nette.ext('redirect', {
        success: function (payload) {
            if (payload.redirect) {
                $.nette.ajax({
                    url: payload.redirect
                });
                return true;
            }
        }
    });
    $.nette.ext('snippets').after(afterRedraw);
    $(window).resize(function () {
        onWindowsSizeChange();
    });

    init();

    function init() {
        demoLogin();
        afterRedraw();
    }

    function afterRedraw() {
        clearTimeout(this.debouncer);
        this.debouncer = setTimeout(function () {
            flashes();
            templateAnimation();
            dialogs();
            scrltp();
            charts();
            adminSort();
            window.footerFix();
        }, 10);
    }

    function onWindowsSizeChange() {
        window.footerFix();
    }

    function demoLogin() {
        var dm = $(".demo-login"),
            opener = dm.find(".opener"),
            openerIcon = opener.find("i");

        opener.on("click", function () {
            dm.toggleClass("open");
            openerIcon.toggleClass("icon-arrow-right").toggleClass("icon-arrow-left");
        });
    }

    function flashes() {
        var time = 4500;
        var els = $('.flash');

        $('.flash-close').click(function () {
            $(this).parent('.flash').fadeOut();
        });
        setTimeout(function () {
            els.fadeOut();
        }, time);
    }

    /**
     * @private
     * @param number
     */
    function animateScrollTo(number) {
        var bodyHeight = $("body").height(),
            windowHeight = window.innerHeight,
            final;
        final = Math.min(number - 80, bodyHeight - windowHeight);
        $("html, body").stop().animate({scrollTop: final}, 500);
    }

    function scrltp() {
        var scrltp = $(".scrltp");
        scrltp.on("click", function () {
            animateScrollTo(0);
        });
        scrltp.disableSelection();

        var afterScroll = function () {
            var scroll = $(window).scrollTop();

            if (scroll > 150 && !scrltp.is(":visible")) {
                scrltp.stop().fadeIn();
            } else if (scroll <= 150 && scrltp.is(":visible")) {
                scrltp.stop().fadeOut();
            }
        };

        var debouncer;

        afterScroll();
        // scroll animation
        $(window).on("scroll", function () {
            clearTimeout(debouncer);
            debouncer = setTimeout(function () {
                afterScroll();
            }, 100);
        });
    }

    function templateAnimation() {
        var input,
            inputType,
            moveInputLabel = function (element) {
                if ($(element).attr("data-label-moved")) return;
                $(element).attr("data-label-moved", "true");
                inputType = $(element).attr('type');
                if (inputType == "file" || inputType == "checkbox") {
                    return;
                }
                $(element).parents('tr').find('th').css({top: '0em', left: '15px'});
                $(element).parents('tr').find('label').css({paddingLeft: '0.5em', fontWeight: 'bold'});
            };

        $('form table td').each(function (index, element) {
            input = $(element).find('input, textarea');
            inputType = input.attr('type');
            if (input.length > 0) {
                if (input.attr("required") && inputType !== "checkbox") {
                    $(element).append("<div class='input-icon' title='Required'><i class='icon-warning'></i></div>");
                    if (inputType == 'file') {
                        $(element).find('.input-icon').css({height: '5.6em', paddingTop: '2.1em'});
                    }
                    $(input).css({paddingLeft: "3.5em"});
                    $(input).parents('tr').find('label').css({paddingLeft: '3.5em'});
                }
                if (inputType == "checkbox") {
                    $(input).parents('tr').find('th').remove();
                }
                if (input.val() != "" || input.attr('placeholder')) {
                    moveInputLabel(input);
                }
            } else {
                moveInputLabel($(element).find('select'));
            }
        });

        $('form table th').on('click', function () {
            $(this).parents('tr').find('td input:first, td textarea:first').focus();
        });


        $('form table td input, form table td textarea').on('focus', function () {
            moveInputLabel(this);
        }).on('change', function () {
            moveInputLabel(this);
        });
    }

    function dialogs() {
        var el = $("<div />"),
            selector = ".dialog",
            questionTag = "data-dialog-q",
            wrapClass = "dialog-wrap",
            iconClass = "icon-warning",
            $elements = $(selector);

        if ($elements.length === 0) {
            return;
        }

        var onClick = function (event) {
            var $wrap = $("<div />").addClass(wrapClass),
                $icon = $("<i >").addClass(iconClass),
                $close = $("<div />").addClass("btn btn-xs btn-visual").text("Ne"),
                $open = $("<a />").addClass("btn btn-xs btn-danger")
                    .text("Ano")
                    .attr("href", $(event.currentTarget).attr("href"))
                    .on("click", function () {
                        el.remove();
                    });

            event.stopPropagation();
            event.preventDefault();

            el.remove();
            $wrap.css({
                top: event.originalEvent.clientY + $(window).scrollTop(),
                left: event.originalEvent.clientX - 282
            });

            $wrap.append($icon);
            $wrap.append($("<p />").text($(event.currentTarget).attr(questionTag)));
            $wrap.append($close);
            $wrap.append($open);

            el = $wrap;
            $("body").append($wrap);
            $.nette.load();
        };

        $elements.unbind("click");
        $elements.removeClass("ajax");
        $elements.on("click", onClick);
        $("body:not(." + wrapClass + ")").on("click", function () {
            el.remove();
        });
    }

    function charts() {
        var element = document.getElementById('myChart');
        if (!element) {
            return;
        }
        var ctx = element.getContext('2d');
        var done = element.getAttribute('data-done');
        var remains = element.getAttribute('data-remains');

        new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Splněno', 'Zbývá'],
                datasets: [{
                    data: [done, remains],
                    backgroundColor: ["#006FAD", "#dddddd"]
                }]
            },
            options: {
                tooltips: {
                    callbacks: {
                        afterLabel: function (tooltipItem, data) {
                            // return "dovedností";
                        }
                    }
                }
            }
        });
    }

    function adminSort() {
        var wrapper = $(".spec-wrap"),
            categories = $(".category:not(.new-category)");

        wrapper.sortable({
            items: ".category:not(.new-category)",
            placeholder: "category-placeholder",
            handle: ".category-handler",
            update: function () {
                var sorted = wrapper.sortable("toArray", {
                    attribute: "data-id"
                });

                $.nette.ajax({
                    url: wrapper.data("category-sort-url"),
                    data: "ids=" + encodeURI(sorted)
                });
            }
        });

        categories.sortable({
            items: ".detail",
            handle: ".spec-handler",
            update: function (event, ui) {
                var sorted = ui.item.parents(".category").sortable("toArray", {
                    attribute: "data-id"
                });

                $.nette.ajax({
                    url: wrapper.data("spec-sort-url"),
                    data: "ids=" + encodeURI(sorted)
                });
            }
        });
    }
});