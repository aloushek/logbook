<?php
namespace App\Model;

use Nette;
use Nette\Security as NS;
use Tracy\Debugger;

/**
 * Class Stag
 * @package App\Model
 */
class Stag {
    use Nette\SmartObject;

    /**
     * @var string
     */
    private $apiUrl = "https://stag-demo.zcu.cz/ws/services/rest2/";

    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * @var string
     */
    protected $ticket;

    /**
     * Set database
     * @param Nette\Database\Context $db
     */
    public function __construct(Nette\Database\Context $db){
        $this->db = $db;
    }

    /**
     * @param string $ticket
     */
    public function setTicket($ticket) {
        $this->ticket = $ticket;
    }

    /**
     * @param number $userId
     */
    public function loadTicket($userId) {
        $user = $this->db->table("users")->where("id", $userId)->fetch();
        if ($user) {
            $this->ticket = $user->ticket;
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws \Exception
     */
    private function sendRequest($url) {
        $s = curl_init();

        curl_setopt($s,CURLOPT_URL, $url."&outputFormat=JSON");
        curl_setopt($s, CURLOPT_USERPWD, $this->ticket.':'."");
        curl_setopt ($s, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($s, CURLOPT_RETURNTRANSFER , true);
        $content = curl_exec($s);
        $status = curl_getinfo($s,CURLINFO_HTTP_CODE);
        curl_close($s);

        if ($status == 500) {
            throw new \Exception($content);
        }

        if ($status === 401) {
            $exception = new Nette\Application\BadRequestException("", 401);
            throw $exception;
        }

        Debugger::barDump($url);
        Debugger::barDump(json_decode($content));
        return $content;
    }

    /**
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    private function process($url) {
        $response = $this->sendRequest($this->apiUrl . $url);
        $decoded = json_decode($response, true);
        if ($decoded) {
            return $decoded;
        }
        return $response;
    }

    private function makeUrl($base, $args, $loginNeeded = false) {
        $first = true;
        if (!$args) {
            $args = [];
        }
        if ($loginNeeded) {
            $args["ticket"] = $this->ticket;
        }

        $base .= "?";
        foreach ($args as $key => $val) {
            if (!$first) {
                $base .= "&";
            }
            $base .= $key . "=" . urlencode($val);
            $first = false;
        }

        return $base;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getUserInfo() {
        $externalLogin = null;
        $fullName = null;

        //user role info
        $url = $this->makeUrl("help/getStagUserForActualUser", [], true);
        $userInfo = $this->process($url);

        //resolve external login & fullname
        if ($userInfo["ucitIdno"]) {
            $urlLogin = $this->makeUrl("users/getExternalLoginByUcitIdno", [
                "ucitIdno" => $userInfo["ucitIdno"]
            ]);
            $urlUcitel = $this->makeUrl("ucitel/getUcitelInfo", [
                "ucitIdno" => $userInfo["ucitIdno"]
            ]);
            $externalLogin = $this->process($urlLogin);
            $fullName = $this->getFullName($this->process($urlUcitel));
        } else if ($userInfo["userName"]) {
            $urlLogin = $this->makeUrl("users/getExternalLoginByOsobniCislo", [
                "osCislo" => $userInfo["userName"]
            ]);
            $externalLogin = $this->process($urlLogin);

            $urlStudent = $this->makeUrl("student/getStudentInfo", [
                "osCislo" => $userInfo["userName"]
            ]);
            $fullName = $this->getFullName($this->process($urlStudent));
        }

        return [$userInfo, $externalLogin, $fullName];
    }

    /**
     * @param $userInfo
     * @return string
     */
    public function getFullName($userInfo) {
        $name = "";

        if($userInfo["titulPred"]) {
            $name .= $userInfo["titulPred"];
            $name .= " ";
        }

        $name .= $userInfo["jmeno"];
        $name .= " ";
        $name .= $userInfo["prijmeni"];

        if($userInfo["titulZa"]) {
            $name .= ", ";
            $name .= $userInfo["titulZa"];
        }

        return $name;
    }

    /**
     * @param string $class
     * @param string $lang
     * @return mixed
     * @throws \Exception
     */
    public function getClassDetail($class, $lang = "cs") {
        $classArray = explode("/", $class);
        $url = $this->makeUrl("predmety/getPredmetInfo",[
            "katedra" => $classArray[0],
            "zkratka" => $classArray[1],
            "lang" => $lang
        ]);

        return $this->process($url);
    }

    /**
     * @param string $surname
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function searchStudents($surname, $name) {
        $params = [];
        if ($surname) {
            $params["prijmeni"] = $surname;
        }
        if ($name) {
            $params["jmeno"] = $name;
        }

        if (count($params) > 0) {
            $url = $this->makeUrl("student/najdiStudentyPodleJmena", $params, true);
            return $this->process($url);
        } else {
            return [];
        }
    }

    /**
     * @param string $class
     * @return mixed
     * @throws \Exception
     */
    public function getStudentsForClass($class) {
        $classArray = explode("/", $class);

        $url = $this->makeUrl("student/getStudentiByPredmet",[
            "katedra" => $classArray[0],
            "zkratka" => $classArray[1]
        ], true);

        return $this->process($url);
    }

    /**
     * @param string $ucitIdno
     * @return mixed
     * @throws \Exception
     */
    public function getSubjectsForTeacher($ucitIdno) {
        $url = $this->makeUrl("predmety/getPredmetyByUcitel",[
            "ucitIdno" => $ucitIdno,
        ]);

        return $this->process($url);
    }

    public function getUserDetail() {
//         return $this->sendRequest("https://stagservices.upol.cz/ws/services/rest/student/getStudentiByPredmet?katedra=KMI&zkratka=PG");
//        return $this->sendRequest("https://stagservices.upol.cz/ws/services/rest/ucitel/getUcitelePredmetu?katedra=KMI&zkratka=KOM");

    }
    
}
