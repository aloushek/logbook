<?php
require __DIR__ . '/cfg.php';

$db = new mysqli($cfg["server"],$cfg["user"],$cfg["pw"],$cfg["db"]);
$start = intval(file_get_contents(__DIR__."/lock.txt"));

$files = scandir(__DIR__ . "/updates/");
$updates = [];
foreach($files as $file){
    if(!in_array($file,[".",".."])){
        $updates[] = intval($file);
    }
}

sort($updates);
$last = $start;
foreach($updates as $update){
    if($start < $update){
        echo $update . " ";
        $sqlSource = file_get_contents(__DIR__."/updates/" . $update . ".sql");
        $db->multi_query($sqlSource);
        while ($db->next_result()) {;}
        $last = $update;
    }
}
$lock = fopen(__DIR__ . "/lock.txt", "w");
fwrite($lock,$last);
fclose($lock);


