<?php
namespace App\Components;

use Nette\Application\UI\Control;

class contentComponentFactory extends Control
{
    /**
     * @var \App\Model\Main
     */
    public $model;

    /**
     * Render template
     * @param integer $id
     * @param bool $withEditor
     */
    public function render($id, $withEditor = false) {
        $template = $this->template;
        $template->setFile(__DIR__ . '/content.latte');
        $template->id = $id;
        $template->withEditor = $withEditor;
        $template->text = $this->getContentById($id);

        $template->render();
    }

    private function getContentById($id) {
        return $this->model->getContentById($id);
    }
}