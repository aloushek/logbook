<?php
namespace App\Model;

use Nette;

/**
 * Class Base
 * @package App\Model
 */
class Main {
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    protected $db;

    /**
     * @var null|array
     */
    private $contentRows;

    /**
     * Set database
     * @param Nette\Database\Context $db
     */
    public function __construct(Nette\Database\Context $db){
        $this->db = $db;
    }

    /**
     * @param $id
     * @return Nette\Database\Table\IRow
     */
    public function getUserById($id){
        return $this->db->table('users')->get($id);
    }

    /**
     * @return array|Nette\Database\Table\IRow[]
     */
    public function getUsers(){
        return $this->db->table('users')->fetchAll();
    }

    /**
     * @param string $stagName
     * @param string $osCislo
     * @param string $stagRole
     * @param string $ucitIdno
     * @param string $fullName
     * @param string $stagTicket
     * @return number ID of user.
     */
    public function getUser($stagName, $osCislo, $stagRole, $ucitIdno = null, $fullName = "", $stagTicket = null) {
        /** @noinspection PhpUndefinedFieldInspection */
        $user = $osCislo ?
            $this->getUserByOsCislo($osCislo):
            $this->getUserByStagName($stagName);
        if ($user) {
            $toUpdate = ["role" => $stagRole];

            if ($stagTicket) {
                $toUpdate["ticket"] = $stagTicket;
            }
            if ($fullName) {
                $toUpdate["fullname"] = $fullName;
            }
            $this->updateUser($user->id, $toUpdate);
        } else {
            $this->createUser($stagName, $osCislo, $stagRole, $ucitIdno);
            return $this->getUser($stagName, $osCislo, $stagRole, $ucitIdno, $fullName, $stagTicket);
        }

        return $user->id;
    }

    /**
     * Update data of user.
     * @param number $id
     * @param array $data
     */
    public function updateUser($id, $data) {
        $this->db->table("users")->where("id", $id)->update($data);
    }

    /**
     * @param string $stagName
     * @return Nette\Database\Table\IRow
     */
    public function getUserByStagName ($stagName) {
        return $this->db->table('users')->where("stagName", $stagName)->fetch();
    }
    /**
     * @param string $osCislo
     * @return Nette\Database\Table\IRow
     */
    public function getUserByOsCislo ($osCislo) {
        return $this->db->table('users')->where("osCislo", $osCislo)->fetch();
    }
    /**
     * @param string $filtr
     * @return Nette\Database\Table\IRow[]
     */
    public function findUsersByFullname ($filtr) {
        return $this->db->table('users')
            ->where("fullname LIKE", "%".$filtr."%")
            ->where("specYear IS NOT NULL")
            ->where("active", 1)
            ->fetchAll();
    }

    /**
     * @param string $stagName
     * @param string $osCislo
     * @param string $role
     * @param string $ucitIdno
     * @return number
     */
    public function createUser ($stagName, $osCislo, $role, $ucitIdno) {
        $insert = $this->db->table("users")->insert([
            "stagName" => $stagName,
            "osCislo" => $osCislo,
            "role" => $role,
            "ucitIdno" => $ucitIdno])->id;
        return $insert;
    }

    public function getSpecYear($userId) {
        return $this->db->table("users")->where("id", $userId)->fetchField("specYear");
    }

    /**
     * @param integer $id ID of post in database.
     * @return Nette\Database\IRow
     */
    public function getContentById($id) {
        if(isset($this->contentRows)){
            if (isset($this->contentRows[$id])) {
                return $this->contentRows[$id];
            } else {
                $this->updateContentById($id, "");
                return (object)["text" => ""];
            }
        }
        $this->contentRows = $this->db->table('content')->select("id, text")->fetchAll();
        return isset($this->contentRows[$id]) ? $this->contentRows[$id] : $this->getContentById($id);
    }

    /**
     * @param integer $id
     * @param string $text
     */
    public function updateContentById($id, $text) {
        $found = $this->db->table("content")->get($id);

        if ($found) {
            $this->db->table('content')->where("id", $id)->update(["text" => $text]);
        } else {
            $this->db->table('content')->insert(["id" => $id, "text" => $text]);
        }
    }
}
