<?php

namespace App\Components;


use App\Model\Main;

trait contentComponentFactoryTrait {
    /**
     * @var contentComponentFactory
     */
    protected $contentComponentFactory;

    /**
     * @param contentComponentFactory $contentComponentFactory
     * @param Main $mainModel
     */
    public function injectContentComponentFactory(contentComponentFactory $contentComponentFactory, Main $mainModel) {
        $this->contentComponentFactory = $contentComponentFactory;
        $this->contentComponentFactory->model = $mainModel;
    }
}